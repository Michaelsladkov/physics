# This Page contains some materials for physics practice in CSE bachelor program of ITMO University

All the guides and tasks can be found on [https://study.physics.itmo.ru](https://study.physics.itmo.ru/course/view.php?id=74).


Google sheet with my calculations is [here](https://docs.google.com/spreadsheets/d/1ZEyXb_iE8wxBse38VDeu8YsYrkfOuOM8iv0RdLNqG7E/edit?usp=sharing).